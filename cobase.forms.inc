<?php

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: cobase.forms.inc
 * .
 */


/**
 * Implements hook_theme_suggestions_form_alter().
 *
 * @noinspection PhpUnused
 */
function cobase_theme_suggestions_form_alter(array &$suggestions, array $variables, $hook): void {
  $elem = $variables['element'];
  $suggestions[] = sprintf('%s__%s', $hook, $elem['#form_id']);
}
