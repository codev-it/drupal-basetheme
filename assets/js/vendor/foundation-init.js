/**
 * @file
 * Default app javascript
 */
(function ($, Drupal, window) {
  var behaviors = Drupal.behaviors;

  /**
   * Run foundation
   *
   * @type {{attach: function}}
   */
  behaviors.appFoundation = {
    attach: function attach(context) {
      var contextOnce;
      if (context instanceof Document) {
        contextOnce = once('appFoundation', 'body', context);
      } else {
        contextOnce = once('appFoundation', $(context));
      }
      contextOnce.forEach(function (elem) {
        $(elem).foundation();
      });
    }
  };
})(jQuery, Drupal, window);
