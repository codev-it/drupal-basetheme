/**
 * @file
 * Default app javascript
 */
(($, Drupal, window) => {
  const { behaviors } = Drupal;

  /**
   * Run foundation
   *
   * @type {{attach: function}}
   */
  behaviors.appFoundation = {
    attach: (context) => {
      let contextOnce;
      if (context instanceof Document) {
        contextOnce = once('appFoundation', 'body', context);
      } else {
        contextOnce = once('appFoundation', $(context));
      }
      contextOnce.forEach((elem) => {
        $(elem).foundation();
      });
    }
  };
})(jQuery, Drupal, window);
