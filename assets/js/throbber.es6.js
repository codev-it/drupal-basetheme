/**
 * @file
 * Default throbber javascript
 */
(($, Drupal) => {
  Drupal.theme.ajaxProgressIndicatorFullscreen = () => `
      <div class="ajax-spinner ajax-spinner--fullscreen">
        <div class="spinner-cn">
          <div class="spinner"></div>
        </div>
      </div>
    `;
})(jQuery, Drupal);

