<?php

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Template\Attribute;
use Drupal\node\NodeInterface;
use Drupal\search\SearchPageInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: cobase.theme
 * .
 */

/**
 * Load parts
 */
require 'cobase.forms.inc';

/**
 * Implements hook_page_attachments_alter().
 *
 * @noinspection PhpUnused
 */
function cobase_page_attachments_alter(array &$attachments): void {
  // include foundation if not as sub theme used
  $theme_name = Drupal::service('theme.manager')->getActiveTheme()->getName();
  if ($theme_name == 'cobase') {
    $attachments['#attached']['library'][] = 'cobase/foundation';
  }
}

/**
 * Implements hook_preprocess_HOOK().
 *
 * @noinspection PhpUnused
 */
function cobase_preprocess_html(&$variables): void {
  $request = Drupal::routeMatch();
  $node = $request->getParameter('node');
  $variables['node_id'] = $node instanceof NodeInterface ? $node->id() : '';

  // Add language body class.
  $variables['attributes']['class'][] = 'lang-' . Drupal::languageManager()
      ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
}

/**
 * Implements template_preprocess_page().
 *
 * @noinspection PhpUnused
 */
function cobase_preprocess_page(&$variables): void {
  // check if error page
  $route_name = Drupal::routeMatch()->getRouteName();
  $variables['is_401'] = $route_name == 'system.401';
  $variables['is_403'] = $route_name == 'system.403';
  $variables['is_404'] = $route_name == 'system.404';

  $variables['main_content_attributes'] = new Attribute();
  $variables['sidebar_attributes'] = new Attribute();
}

/**
 * Implements hook_preprocess_breadcrumb().
 *
 * @noinspection PhpUnused
 */
function cobase_preprocess_breadcrumb(&$variables): void {
  $route_match = Drupal::routeMatch();
  $entity = $route_match->getParameter('entity');
  if ($entity instanceof SearchPageInterface) {
    $variables['current_page_title'] = $entity->getPlugin()->suggestedTitle();
  }
  else {
    if ($route_match->getRouteObject()) {
      $variables['current_page_title'] = Drupal::service('title_resolver')
        ->getTitle(Drupal::request(), $route_match->getRouteObject());
    }
    else {
      $variables['current_page_title'] = '';
    }
  }
  $variables['#cache']['contexts'][] = 'url';
}

/**
 * Implements hook_preprocess_block().
 *
 * @noinspection PhpUnused
 */
function cobase_preprocess_block(&$variables): void {
  $elements = $variables['elements'];
  if ($elements['#plugin_id'] == 'inline_block:basic') {
    /** @var Drupal\block_content\Entity\BlockContent $block */
    $block = $elements['content']['#block_content'];
    $quick_edit_id = 'block_content/' . $block->id();
    $variables['attributes']['data-quickedit-entity-id'] = $quick_edit_id;
  }
}

/**
 * Implements hook_preprocess_links().
 *
 * @noinspection PhpUnused
 */
function cobase_preprocess_links(&$variables): void {
  $links = $variables['links'] ?? [];
  switch ($variables['theme_hook_original']) {
    case 'links__dropbutton':
    case 'links__dropbutton__operations':
      $first_button = array_values($links)[0]['link'] ?? [];
      $first_button['#options']['attributes']['class'][] = 'button';
      $first_button['#options']['attributes']['class'][] = 'tiny';
      $variables['dropdown_id'] = uniqid();
      $variables['first_button'] = $first_button;
      break;
  }
}

/**
 * Implements hook_banner_options_alter().
 *
 * @noinspection PhpUnused
 */
function cobase_banner_options_alter(&$variables): void {
  $variables = [
    'prevArrow' => '<button class="slick-prev" aria-label="Previous" type="button"><i class="fas fa-caret-left"></i></button>',
    'nextArrow' => '<button class="slick-next" aria-label="Next" type="button"><i class="fas fa-caret-right"></i></button>',
  ];
}
